/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "spooky.h"
#include "SpookyV2.h"

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#undef NDEBUG
#include <assert.h>

static bool check128(
	const void *msg, size_t len, uint64_t seed_lo, uint64_t seed_hi)
{
	uint64_t my[2];
	spooky128(my, msg, len, seed_lo, seed_hi);

	uint64_t ref[] = { seed_lo, seed_hi };
	SpookyHash::Hash128(msg, len, &ref[0], &ref[1]);

	return ref[0] == my[0] && ref[1] == my[1];
}

static bool check64(const void *msg, size_t len, uint64_t seed)
{
	return spooky64(msg, len, seed) == SpookyHash::Hash64(msg, len, seed);
}

static bool check32(const void *msg, size_t len, uint32_t seed)
{
	return spooky32(msg, len, seed) == SpookyHash::Hash32(msg, len, seed);
}

static bool done()
{
	exit(EXIT_SUCCESS);
}

int main()
{
	{
		char author[] = "Christoph Gärtner";
		assert(check128(author, sizeof author, 0xF372972BA, 0x687EC678));
		assert(check64(author, sizeof author, 0xFE6053));
		assert(check32(author, sizeof author, 42));
	}

	{
		char noinit[191]; // can't use 192 because of off-by-one in Hash128
		assert(check128(noinit, sizeof noinit, 12345, 6789));
		assert(check64(noinit, sizeof noinit, 0x123EF));
		assert(check32(noinit, sizeof noinit, 8043));
	}

	{
		char noinit[1027];
		assert(check128(noinit, sizeof noinit, 7878, 23232));
		assert(check64(noinit, sizeof noinit, 10010));
		assert(check32(noinit, sizeof noinit, 0x77E32A21));
	}

	{
		int data[256];

		srand((unsigned)time(NULL));
		for(size_t i = 0; i < sizeof data / sizeof *data; ++i)
			data[i] = rand();

		assert(check128(data, sizeof data, (uint64_t)rand(), (uint64_t)rand()));
		assert(check64(data, sizeof data, (uint64_t)rand()));
		assert(check32(data, sizeof data, (uint32_t)rand()));
	}

	{
		assert(done());
	}

	return EXIT_FAILURE;
}
