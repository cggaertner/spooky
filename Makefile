CC := gcc
CXX := g++
DFLAGS :=
CFLAGS := -std=c99 -O3
CXXFLAGS := -std=c++98 -O3
CLANG_CHECK := clang -std=c99 -fsyntax-only -Werror -Weverything

.PHONY : build check clean

build : spooky.o

check : test bench
	./test
	./bench

test bench : % : %.cxx spooky.o SpookyV2.o
	$(CXX) $(CXXFLAGS) -o $@ $^

clean :
	rm -f test bench spooky.o SpookyV2.o

SpookyV2.o : %.o : %.cpp %.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

spooky.o : %.o : %.c %.h
	$(CLANG_CHECK) $(DFLAGS:%=-D%) $<
	$(CC) $(CFLAGS) $(DFLAGS:%=-D%) -c -o $@ $<
