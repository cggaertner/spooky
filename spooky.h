/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef SPOOKY_H_
#define SPOOKY_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" uint64_t *spooky128(uint64_t hash[],
#else
extern uint64_t *spooky128(uint64_t hash[static 2],
#endif
	const void *message, size_t length, uint64_t seed_lo, uint64_t seed_hi);

static inline uint64_t spooky64(
	const void *message, size_t length, uint64_t seed)
{
	uint64_t hash[2];
	return *spooky128(hash, message, length, seed, seed);
}

static inline uint32_t spooky32(
	const void *message, size_t length, uint32_t seed)
{
	uint64_t hash[2];
	return (uint32_t)*spooky128(hash, message, length, seed, seed);
}

#endif
