/*	Copyright 2010, 2011, 2012 Bob Jenkins
	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "spooky.h"

#include <assert.h>
#include <string.h>

#ifndef SPOOKY_STATIC_SEED
#define SPOOKY_STATIC_SEED 0xDEADBEEFDEADBEEF
#endif

enum
{
	BLOCK_SIZE = 12 * sizeof (uint64_t),
	SPOOKY_SHORT_MAX = 2 * BLOCK_SIZE
};

static inline uint64_t rotl64(uint64_t x, int k)
{
	return (x << k) | (x >> (64 - k));
}

static inline void mix_short(
	uint64_t *restrict a, uint64_t *restrict b, uint64_t *restrict c,
	uint64_t *restrict d)
{
	*c = rotl64(*c, 50); *c += *d; *a ^= *c;
	*d = rotl64(*d, 52); *d += *a; *b ^= *d;
	*a = rotl64(*a, 30); *a += *b; *c ^= *a;
	*b = rotl64(*b, 41); *b += *c; *d ^= *b;
	*c = rotl64(*c, 54); *c += *d; *a ^= *c;
	*d = rotl64(*d, 48); *d += *a; *b ^= *d;
	*a = rotl64(*a, 38); *a += *b; *c ^= *a;
	*b = rotl64(*b, 37); *b += *c; *d ^= *b;
	*c = rotl64(*c, 62); *c += *d; *a ^= *c;
	*d = rotl64(*d, 34); *d += *a; *b ^= *d;
	*a = rotl64(*a,  5); *a += *b; *c ^= *a;
	*b = rotl64(*b, 36); *b += *c; *d ^= *b;
}

static inline void burn_short(
	uint64_t *restrict a, uint64_t *restrict b, uint64_t *restrict c,
	uint64_t *restrict d)
{
	*d ^= *c; *c = rotl64(*c, 15); *d += *c;
	*a ^= *d; *d = rotl64(*d, 52); *a += *d;
	*b ^= *a; *a = rotl64(*a, 26); *b += *a;
	*c ^= *b; *b = rotl64(*b, 51); *c += *b;
	*d ^= *c; *c = rotl64(*c, 28); *d += *c;
	*a ^= *d; *d = rotl64(*d,  9); *a += *d;
	*b ^= *a; *a = rotl64(*a, 47); *b += *a;
	*c ^= *b; *b = rotl64(*b, 54); *c += *b;
	*d ^= *c; *c = rotl64(*c, 32); *d += *c;
	*a ^= *d; *d = rotl64(*d, 25); *a += *d;
	*b ^= *a; *a = rotl64(*a, 63); *b += *a;
}

static uint64_t *hash_short(
	uint64_t hash[static 2], const void *message, size_t length)
{
#ifndef SPOOKY_NOCOPY
	union {
		uint64_t u64[SPOOKY_SHORT_MAX / sizeof (uint64_t)];
		uint32_t u32[SPOOKY_SHORT_MAX / sizeof (uint32_t)];
		uint8_t u8[SPOOKY_SHORT_MAX];
	} buf;

	const uint64_t *p64 = memcpy(buf.u64, message, length);
#else
	const uint64_t *p64 = message;
#endif
	size_t remainder = length % 32;

	uint64_t a = hash[0];
	uint64_t b = hash[1];
	uint64_t c = SPOOKY_STATIC_SEED;
	uint64_t d = SPOOKY_STATIC_SEED;

	if(length > 15)
	{
		const uint64_t *const END = p64 + (length / 32) * 4;

		for(; p64 < END; p64 += 4)
		{
			c += p64[0];
			d += p64[1];
			mix_short(&a, &b, &c, &d);
			a += p64[2];
			b += p64[3];
		}

		if(remainder >= 16)
		{
			c += p64[0];
			d += p64[1];
			mix_short(&a, &b, &c, &d);
			p64 += 2;
			remainder -= 16;
		}
	}

	d += (uint64_t)length << 56;

	const uint8_t *p8 = (const uint8_t *)p64;
	const uint32_t *p32 = (const uint32_t *)p64;

	switch(remainder)
	{
	case 15:
		d += (uint64_t)p8[14] << 48;
	case 14:
		d += (uint64_t)p8[13] << 40;
	case 13:
		d += (uint64_t)p8[12] << 32;
	case 12:
		d += p32[2];
		c += p64[0];
		break;

	case 11:
		d += (uint64_t)p8[10] << 16;
	case 10:
		d += (uint64_t)p8[9] << 8;
	case 9:
		d += (uint64_t)p8[8];
	case 8:
		c += p64[0];
		break;

	case 7:
		c += (uint64_t)p8[6] << 48;
	case 6:
		c += (uint64_t)p8[5] << 40;
	case 5:
		c += (uint64_t)p8[4] << 32;
	case 4:
		c += p32[0];
		break;

	case 3:
		c += (uint64_t)p8[2] << 16;
	case 2:
		c += (uint64_t)p8[1] << 8;
	case 1:
		c += (uint64_t)p8[0];
		break;

	case 0:
		c += SPOOKY_STATIC_SEED;
		d += SPOOKY_STATIC_SEED;
	}

	burn_short(&a, &b, &c, &d);

	hash[0] = a;
	hash[1] = b;

	return hash;
}

static inline void mix_long(
	const uint64_t data[static restrict 12],
	uint64_t *restrict s0, uint64_t *restrict s1, uint64_t *restrict s2,
	uint64_t *restrict s3, uint64_t *restrict s4, uint64_t *restrict s5,
	uint64_t *restrict s6, uint64_t *restrict s7, uint64_t *restrict s8,
	uint64_t *restrict s9, uint64_t *restrict s10, uint64_t *restrict s11)
{
	*s0  += data[ 0]; *s2  ^= *s10; *s11 ^= *s0;  *s0  = rotl64(*s0 , 11); *s11 += *s1; 
	*s1  += data[ 1]; *s3  ^= *s11; *s0  ^= *s1;  *s1  = rotl64(*s1 , 32); *s0  += *s2; 
	*s2  += data[ 2]; *s4  ^= *s0;  *s1  ^= *s2;  *s2  = rotl64(*s2 , 43); *s1  += *s3; 
	*s3  += data[ 3]; *s5  ^= *s1;  *s2  ^= *s3;  *s3  = rotl64(*s3 , 31); *s2  += *s4; 
	*s4  += data[ 4]; *s6  ^= *s2;  *s3  ^= *s4;  *s4  = rotl64(*s4 , 17); *s3  += *s5; 
	*s5  += data[ 5]; *s7  ^= *s3;  *s4  ^= *s5;  *s5  = rotl64(*s5 , 28); *s4  += *s6; 
	*s6  += data[ 6]; *s8  ^= *s4;  *s5  ^= *s6;  *s6  = rotl64(*s6 , 39); *s5  += *s7; 
	*s7  += data[ 7]; *s9  ^= *s5;  *s6  ^= *s7;  *s7  = rotl64(*s7 , 57); *s6  += *s8; 
	*s8  += data[ 8]; *s10 ^= *s6;  *s7  ^= *s8;  *s8  = rotl64(*s8 , 55); *s7  += *s9; 
	*s9  += data[ 9]; *s11 ^= *s7;  *s8  ^= *s9;  *s9  = rotl64(*s9 , 54); *s8  += *s10;
	*s10 += data[10]; *s0  ^= *s8;  *s9  ^= *s10; *s10 = rotl64(*s10, 22); *s9  += *s11;
	*s11 += data[11]; *s1  ^= *s9;  *s10 ^= *s11; *s11 = rotl64(*s11, 46); *s10 += *s0; 
}

static inline void partial_burn_long(
	uint64_t *restrict h0, uint64_t *restrict h1, uint64_t *restrict h2,
	uint64_t *restrict h3, uint64_t *restrict h4, uint64_t *restrict h5,
	uint64_t *restrict h6, uint64_t *restrict h7, uint64_t *restrict h8,
	uint64_t *restrict h9, uint64_t *restrict h10, uint64_t *restrict h11)
{
	*h11 += *h1;  *h2  ^= *h11; *h1  = rotl64(*h1, 44);
	*h0  += *h2;  *h3  ^= *h0;  *h2  = rotl64(*h2, 15);
	*h1  += *h3;  *h4  ^= *h1;  *h3  = rotl64(*h3, 34);
	*h2  += *h4;  *h5  ^= *h2;  *h4  = rotl64(*h4, 21);
	*h3  += *h5;  *h6  ^= *h3;  *h5  = rotl64(*h5, 38);
	*h4  += *h6;  *h7  ^= *h4;  *h6  = rotl64(*h6, 33);
	*h5  += *h7;  *h8  ^= *h5;  *h7  = rotl64(*h7, 10);
	*h6  += *h8;  *h9  ^= *h6;  *h8  = rotl64(*h8, 13);
	*h7  += *h9;  *h10 ^= *h7;  *h9  = rotl64(*h9, 38);
	*h8  += *h10; *h11 ^= *h8;  *h10 = rotl64(*h10, 53);
	*h9  += *h11; *h0  ^= *h9;  *h11 = rotl64(*h11, 42);
	*h10 += *h0;  *h1  ^= *h10; *h0  = rotl64(*h0, 54);
}

static inline void burn_long(
	const uint64_t data[static restrict 12],
	uint64_t *restrict h0, uint64_t *restrict h1, uint64_t *restrict h2,
	uint64_t *restrict h3, uint64_t *restrict h4, uint64_t *restrict h5,
	uint64_t *restrict h6, uint64_t *restrict h7, uint64_t *restrict h8,
	uint64_t *restrict h9, uint64_t *restrict h10, uint64_t *restrict h11)
{
	*h0 += data[0]; *h1 += data[1]; *h2  += data[ 2]; *h3  += data[ 3];
	*h4 += data[4]; *h5 += data[5]; *h6  += data[ 6]; *h7  += data[ 7];
	*h8 += data[8]; *h9 += data[9]; *h10 += data[10]; *h11 += data[11];

	partial_burn_long(h0, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11);
	partial_burn_long(h0, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11);
	partial_burn_long(h0, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11);
}

static uint64_t *hash_long(
	uint64_t hash[static 2], const void *message, size_t length)
{
	uint64_t h0, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11;
	h0 = h3 = h6 = h9 = hash[0];
	h1 = h4 = h7 = h10 = hash[1];
	h2 = h5 = h8 = h11 = SPOOKY_STATIC_SEED;

	uint64_t buf[12];

	size_t remainder = length % BLOCK_SIZE;
	const uint8_t *p8 = message;
	const uint8_t *const END = p8 + (length - remainder);

	for(; p8 < END; p8 += BLOCK_SIZE)
	{
#ifndef SPOOKY_NOCOPY
		memcpy(buf, p8, BLOCK_SIZE);
		mix_long(buf,
			&h0, &h1, &h2, &h3, &h4, &h5, &h6, &h7, &h8, &h9, &h10, &h11);
#else
		mix_long((const uint64_t *)(const void *)p8,
			&h0, &h1, &h2, &h3, &h4, &h5, &h6, &h7, &h8, &h9, &h10, &h11);
#endif
	}

	memcpy(buf, END, remainder);
	memset((uint8_t *)buf + remainder, 0, BLOCK_SIZE - remainder - 1);
	((uint8_t *)buf)[BLOCK_SIZE - 1] = (uint8_t)remainder;

	burn_long(buf,
		&h0, &h1, &h2, &h3, &h4, &h5, &h6, &h7, &h8, &h9, &h10, &h11);

	hash[0] = h0;
	hash[1] = h1;

	return hash;
}

uint64_t *spooky128(
	uint64_t hash[static 2], const void *message, size_t length,
	uint64_t seed_lo, uint64_t seed_hi)
{
	hash[0] = seed_lo, hash[1] = seed_hi;
	return (length <= SPOOKY_SHORT_MAX ? hash_short : hash_long)(
		hash, message, length);
}
