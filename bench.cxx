/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "spooky.h"
#include "SpookyV2.h"

#include <stdio.h>
#include <time.h>

static const char DUMMY_SHORT[190] = { 0 };
static const char DUMMY_LONG[10 * 1024 * 1024] = { 0 };

enum { N_SHORT = 20000000, N_LONG = 1000 };

int main()
{
	{
		uint64_t hash[2] = { 0 };

		clock_t start = clock();
		for(int i = 0; i < N_SHORT; ++i)
		{
			SpookyHash::Hash128(
				DUMMY_SHORT, sizeof DUMMY_SHORT, &hash[0], &hash[1]);
			SpookyHash::Hash128(
				DUMMY_SHORT, sizeof DUMMY_SHORT, &hash[0], &hash[1]);
			SpookyHash::Hash128(
				DUMMY_SHORT, sizeof DUMMY_SHORT, &hash[0], &hash[1]);
		}
		clock_t end = clock();

		printf("SHORT, ref: %ums\n",
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}

	{
		uint64_t hash[2] = { 0 };

		clock_t start = clock();
		for(int i = 0; i < N_SHORT; ++i)
		{
			spooky128(hash, DUMMY_SHORT, sizeof DUMMY_SHORT, 0, 0);
			spooky128(hash, DUMMY_SHORT, sizeof DUMMY_SHORT, 0, 0);
			spooky128(hash, DUMMY_SHORT, sizeof DUMMY_SHORT, 0, 0);
		}
		clock_t end = clock();

		printf("SHORT, my: %ums\n",
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}

	{
		uint64_t hash[2] = { 0 };

		clock_t start = clock();
		for(int i = 0; i < N_LONG; ++i)
		{
			SpookyHash::Hash128(
				DUMMY_LONG, sizeof DUMMY_LONG, &hash[0], &hash[1]);
			SpookyHash::Hash128(
				DUMMY_LONG, sizeof DUMMY_LONG, &hash[0], &hash[1]);
			SpookyHash::Hash128(
				DUMMY_LONG, sizeof DUMMY_LONG, &hash[0], &hash[1]);
		}
		clock_t end = clock();

		printf("LONG, ref: %ums\n",
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}

	{
		uint64_t hash[2] = { 0 };

		clock_t start = clock();
		for(int i = 0; i < N_LONG; ++i)
		{
			spooky128(hash, DUMMY_LONG, sizeof DUMMY_LONG, 0, 0);
			spooky128(hash, DUMMY_LONG, sizeof DUMMY_LONG, 0, 0);
			spooky128(hash, DUMMY_LONG, sizeof DUMMY_LONG, 0, 0);
		}
		clock_t end = clock();

		printf("LONG, my: %ums\n",
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}

	return 0;
}
